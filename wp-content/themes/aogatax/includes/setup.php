<?php
/**
 * General setup for the AOGATAX Theme.
 *
 * @since 0.1.0
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */
namespace AOGATAX\Theme\Setup;

/**
 * Set up theme defaults
 *
 * @since 0.1.0
 * @return void.
 */
function setup() {
	add_action( 'init', __NAMESPACE__ . '\register_menus');
	add_action( 'init', __NAMESPACE__ . '\create_post_types');
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\styles' );
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\scripts' );
	add_action( 'admin_menu', __NAMESPACE__ . '\remove_admin_menu_items' );
}

function register_menus() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
}

function create_post_types() {
	register_post_type('home',
		array(
		  	'labels' => array(
	    		'name' => __( 'Home Content' ),
	        	'singular_name' => __( 'Home Content' ),
	        	'add_new_item' => __( 'Add Home Page Content'),
	      	),
	      	'hierarchical' => true,
	      	'supports' => array('title','date','editor','author', 'thumbnail', 'page-attributes'),
	      	'public' => true,
	      	'has_archive' => true,
	      	'menu_icon' => 'dashicons-admin-comments',
		)
	);
}

function styles() {

	wp_enqueue_style( 'bootstrap', AOGATAX_TEMPLATE_URL . '/assets/css/bootstrap.min.css');
	wp_enqueue_style( 'aogatax', AOGATAX_TEMPLATE_URL . '/style.css');
}

function scripts() {
	//Adds JQuery from Google's CDN. Code pulled from www.http://css-tricks.com/snippets/wordpress/include-jquery-in-wordpress-theme/
    if (!is_admin()) add_action("wp_enqueue_scripts",  __NAMESPACE__ . "\my_jquery_enqueue", 11);

	wp_enqueue_script( 'aogatax', AOGATAX_TEMPLATE_URL . '/includes/scripts.js', array('jquery') );

	// Google Maps Geocoder API
	$key = 'AIzaSyAD2lDtSQk5njYp1pukqXSOjn7VVHfNd1g';
	wp_enqueue_script( 'googlemap_api', 
		"http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . 
		"://maps.googleapis.com/maps/api/js?key=$key&signed_in=true" );
}

function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", false, null);
    wp_enqueue_script('jquery');
}

function remove_admin_menu_items() {

	$remove_menu_items = array(__('Posts'), __('Comments'));
	global $menu;
	end ($menu);

	while (prev($menu)){

		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
			unset($menu[key($menu)]);
		}
	}
}