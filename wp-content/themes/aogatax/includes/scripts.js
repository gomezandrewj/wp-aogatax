(function () {
	
	var apiData;
	var geocoder;
	
	$(document).ready(function(){


		// Issue click action
		//

		$('div.aogatax-issues-section').click(function() {

			$(this).css('background','url(./wp-content/themes/aogatax/assets/images/issue-click.png) bottom no-repeat');
			$(this).siblings().css('background','');

			$('div.aogatax-issues-content div').html($(this).children('div.aogatax-issue-content').html());

		});
		
		// Window popup for share buttons
		$('img#share').click(function() {

			var NWin = window.open($(this).attr('href'), '', 'scrollbars=1,height=400,width=400');
			
			if (window.focus) {
				NWin.focus();
			}
		
			return false;
		
		});

		// Pop up question scripts
		//

		var MOBILE_WIDTH = '620';

	    if (sessionStorage.getItem('popup') == 'close') {
	        $(".popup-question-div").hide();
	    } else {
		    $(".popup-question-div").show(100);
	    }

	    if ($(window).width() > MOBILE_WIDTH) {
	        $(".popup-question-div form").show(100);
	        $(".popup-question-div form li").hide();
	        $(".popup-question-div form li.textarea").show();
	    }

	    $(".popup-close-question-top").click(function(){
	    
	        $(".popup-question-div").hide(100);

	        sessionStorage.setItem('popup', 'close');

	    });

	    $(".popup-submit-question-top").click(function(){

	    	if ($(".popup-question-div form").css('display') == 'none' || $(".popup-question-div").css('height') == '55px' || $(".popup-question-div").css('height') == '50px') {
	        	$(".popup-question-div form").show(100);
	            $(".popup-question-div form li").hide();
	            $(".popup-question-div form li.textarea").show();

	            if ($(window).width() <= MOBILE_WIDTH) {
	                $(".popup-question-div form li").show();
	                $(".popup-question-div img.popup-disclaimer").css('display','block');
	                $(".popup-question-div").css('height','100%');
	            } else {
	                $(".popup-question-div").css('height','275px');
	            }
	        } else {
	        	$(".popup-question-div img.popup-disclaimer").hide(100);
	            $(".popup-question-div form").hide(100);

	            if ($(window).width() <= MOBILE_WIDTH) {
	                $(".popup-question-div").css('height','50px');
	            } else {
	                $(".popup-question-div").css('height','55px');
	            }	
	        }
	    });

	    $(".popup-question-div li.textarea textarea").click(function(){

	        if ($(window).width() > MOBILE_WIDTH) {
	            $(".popup-question-div form li").show();
	            $(".popup-question-div img.popup-disclaimer").show();
	            $(".popup-question-div").css('height','725px');
	        }

	    });

		// Email text update
		//

		updateNameHTML();
		$('input#input_1_1, input#input_1_3, input#input_1_6, input#input_1_7').change(updateNameHTML);

		// Email form validation
		//

		$('input#input_1_5, input#input_1_10').change(function() {

			var email = $('input#input_1_5').val();
			var confirmEmail = $('input#input_1_10').val();

			if(email != confirmEmail) {
				formResults('danger','Emails do not match.');
			} else {
				formResults('success','');
			}

		});
		
		geocoder = new google.maps.Geocoder();

		// call API on address change
		//
		if ($('input#input_1_2').val() || $('input#input_1_6').val() || $('input#input_1_7').val()) {
			callAPI();
		}
		$('input#input_1_2, input#input_1_6, input#input_1_7').change(callAPI);

		// update message on option change
		//

		var message = $('div.aogatax-message').html();
		var customMessage = $('textarea#input_1_16').html();

		if (customMessage != undefined) {
			$('div.aogatax-message').html(message + "<br>" + customMessage);
		} else {
			$('div.aogatax-message').html(message);
		}

		setHiddenValues();

		$('input#choice_1_17_0, input#choice_1_17_1, input#choice_1_17_2').change(function() {
			
			message = $('div.aogatax-message-' + $(this).val()).html();

			if (customMessage != undefined) {
				$('div.aogatax-message').html(message + "<p>" + customMessage + "</p>");
			} else {
				$('div.aogatax-message').html(message);
			}

		});

		// update message on personalized message change
		//

		$('textarea#input_1_16').change(function() {
			
			customMessage = $(this).val();

			$('div.aogatax-message').html(message + "<p>" + customMessage + "</p>");

		});

		// update hidden values on input change.
		//

		$('input, textarea').change(setHiddenValues);

		// add option messages to form on ready
		//

		$('li.gchoice_1_17_0').append($('div.aogatax-message-1').html());
		$('li.gchoice_1_17_1').append($('div.aogatax-message-2').html());
		$('li.gchoice_1_17_2').append($('div.aogatax-message-3').html());

	});

	function updateNameHTML() {

		var firstName = $('input#input_1_1').val();
		var lastName = $('input#input_1_3').val();
		var city = $('input#input_1_6').val(); 
		var state = $('input#input_1_7').val();

		if (firstName != "" && lastName != "" && $('div#aogatax-user-name').text() != firstName + " " + lastName) {
			$('div#aogatax-user-name').html(firstName + " " + lastName);
		}
		
		if (city != "" && state != "" && $('div#aogatax-user-location').text() != city + ", " + state) {
			$('div#aogatax-user-location').html(city + ", " + state);
		}

	}

	function callAPI() {

		var street = $('input#input_1_2').val();
		var city = $('input#input_1_6').val();
		var state = $('input#input_1_7').val(); 

		if (state != "") {
			var address = street + " " + city + ", " + state;
			geocodeAddress(geocoder, address);
		}
	}

	function setHiddenValues() {

		if (apiData !== undefined) {
			$('div#aogatax-legislator-name').html("Dear " + apiData[0].full_name + ",");
		}
		
		$('input#input_1_12').attr('value',$('div.aogatax-email-text').html());

		if (apiData !== undefined) {
			$('div#aogatax-legislator-name').html("Dear " + apiData[1].full_name + ",");
		}

		$('input#input_1_14').attr('value',$('div.aogatax-email-text').html());
	}

	function formResults(result, message) {

		if(result == 'danger') {
			$('div.aogatax-form_wrapper input[type=submit]').hide();
		}
		else {
			$('div.aogatax-form_wrapper input[type=submit]').show();
		}

		$('div.aogatax-api-results').html('<p class="alert bg-'+result+'">'+message+'</p>');
		
	}

	// Google Maps Geocoder API 
	function geocodeAddress(geocoder, address) {

		geocoder.geocode({'address': address}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				var geoLocation = results[0].geometry.location;
				getLegislators(geoLocation);
			} 
			else {
				formResults('danger', 'Please verify your address. Geocode was not successful: ' + status);
			}
		});
	}

	// Open States API
	function getLegislators(location) {

		const url = 'http://openstates.org/api/v1/'
		const openStatesApiKey = '9189700b6da24ca7be85084e6f2584ce';
		const method = '/legislators/geo/' 
							+ '?lat=' + location.lat() 
							+ '&long=' + location.lng()
							+ '&apikey=' + openStatesApiKey;

		$.ajax( url + method )

			.success(function( data ) {

				if(data[0] == null) {
					formResults('danger', "Please verify your address. Could not locate a legislator in your area. "
									 + status );
					return null;
				}

				formResults('success','');
				setLegislatorInfo(data);
			})

			.fail(function( jqXHR, status, message ) {
				formResults('danger', "Please verify your address. Could not locate a legislator in your area. "
									 + status + ": " + message);
			});
	}

	function setLegislatorInfo(data) {

		apiData = data;
		// Verify emails exist

		// Update html email text
		// need to clean this up
		$('div#aogatax-legislator-name').html("Dear " + data[0].full_name + ",");
		$('div#aogatax-legislator-name').css('font-size','20px');
		$('div#aogatax-legislator-name').animate({
			'font-size': '14px'
		}, 400);

		// Set hidden form values
		if( typeof data[0].email != 'undefined' && data[0].email != "") {
			$('input#input_1_11').attr('value',data[0].email); 
		}
		else {
			$('input#input_1_11').attr('value','undefined'); 
		}

		if( typeof data[1].email != 'undefined' && data[1].email != "") {
			$('div#aogatax-legislator-name').html("Dear " + data[1].full_name + ",");
			$('input#input_1_13').attr('value',data[1].email); 
		}
		else {
			$('input#input_1_13').attr('value','undefined'); 
		}

		if( (typeof data[0].email == 'undefined' || data[0].email == "") 
			&& (typeof data[1].email == 'undefined' || data[1].email == "") ) {
			formResults('warning', 'Your legislators do not have emails listed, but you may sign the petition anyway.');
		}

		setHiddenValues();
	}
})();