<?php
/**
 * Mail after Gform submit
 *
 * @since 0.1.0
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */
namespace AOGATAX\Theme\Mail;

function setup() {

	add_action( 'gform_after_submission', __NAMESPACE__ . '\mail', 10, 2);
	add_action( 'gform_after_submission', __NAMESPACE__ . '\newsletter_signup', 10, 2);
}

function mail($entry, $form) {

	// Send mail to legislature
	$from = 'From: ' . rgar( $entry, '1') . ' ' . rgar( $entry, '3' ) . ' <' . rgar( $entry, '5' ) . '>';

	send(rgar( $entry, '11' ), $from, $entry[12]);
	send(rgar( $entry, '13' ), $from, $entry[14]);

}

function send($to, $from, $message) {

	if( strpos($to,'@') > 0 ) {
		$subject = "Keep Alaska Open for Business";
		$headers[] = $from;
		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		wp_mail( $to, $subject, $message, $headers );
	}
}

function newsletter_signup($entry, $form) {

	if( $entry['9.1'] != "" ) {
		require_once 'MailChimp.php';
		$MailChimp = new \Drewm\MailChimp('fcf106222e6785b88140bfe1e6ff6032-us10');

		$result = $MailChimp->call('lists/subscribe', array(
		                'id'                => '52364772f9',
		                'email'             => array('email'=>$entry[5]),
		                'merge_vars'        => array('FNAME'=>$entry[1], 'LNAME'=>$entry[3]),
		                'update_existing'   => true,
		            ));
	}

}