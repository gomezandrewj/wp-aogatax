<?php
/**
 * The main template file
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */

get_header(); ?>

	<div class="clearfix"></div>
	<main class="aogatax-main">
	
		<div class="aogatax-section aogatax-page text-center">
			<?php  
				while ( have_posts() ) : the_post();
			  		the_content();
				endwhile;
			?>
		</div>
	
	</main>

<?php 
get_footer();
?>