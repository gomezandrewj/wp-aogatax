<?php
/**
 * The front-page template file
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */

get_header(); ?>

    <div class="clearfix"></div>
    <main class="aogatax-main">

        <div class="aogatax-title-text aogatax-section text-center">
            <?php  
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'title-text' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            ?>
        </div>

        <img src="<?php echo AOGATAX_IMAGES . 'SnowGraphic.png'; ?>" width="100%" />
        
        <div class="aogatax-section col-xs-12">
                <?php 
                    $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'form-text' ) );
                    while ( $query->have_posts() ) : $query->the_post();
                        the_content();
                    endwhile;
                ?>
            <div class="aogatax-api-results"></div>
            <?php gravity_form(1, false, false, false, '', false); ?>
        </div>

        <div class="aogatax-email-text aogatax-section cpl-xs-12">
            <div id="aogatax-legislator-name">Dear [Legislator Name],</div>
            <br>
            <div class="aogatax-message">
            <?php 
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'email-text' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            ?>
            </div>
            <br>
            Sincerely,
            <div id="aogatax-user-name">[Autofill, Name]</div>
            <div id="aogatax-user-location">[Autofill, location]</div>
        </div>

        <div class="aogatax-message-1 hidden">
            <?php 
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'email-text' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            ?>
        </div>

        <div class="aogatax-message-2 hidden">
            <?php 
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'email-text-2' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            ?>
        </div>

        <div class="aogatax-message-3 hidden">
            <?php 
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'email-text-3' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            ?>
        </div>

        <div class="aogatax-issues aogatax-section text-center col-xs-12">
            <?php 
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'issues-text' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            ?>
            <div id="aogatax-issue-1" class="aogatax-issues-section col-md-4 col-sm-12">
                <img src="<?php echo AOGATAX_IMAGES . 'tax-credits.png'; ?>" />
                <?php 
                    $query = new WP_Query( array('post_type' => 'home', 'name' => 'tax-credits' ) );
                    while ( $query->have_posts() ) : $query->the_post();
                        the_title('<h2>', '</h2>');
                        ?>
                            <div class="aogatax-issue-content">
                                <?php 
                                    the_content(); 
                                ?>
                                <a href="tax-credits">Learn More</a>
                            </div>
                        <?php 
                    endwhile;
                ?>
            </div>

            <div id="aogatax-issue-2" class="aogatax-issues-section col-md-4 col-sm-12">
                <img src="<?php echo AOGATAX_IMAGES . 'minimum-production-tax.png'; ?>" />
                <?php 
                    $query = new WP_Query( array('post_type' => 'home', 'name' => 'minimum-production-tax' ) );
                    while ( $query->have_posts() ) : $query->the_post();
                        the_title('<h2>', '</h2>');
                        ?>
                            <div class="aogatax-issue-content">
                                <?php
                                    the_content(); 
                                ?>
                                <a href="minimum-production-tax">Learn More</a>
                            </div>
                        <?php 
                    endwhile;
                ?>
            </div>
            <div id="aogatax-issue-3" class="aogatax-issues-section col-md-4 col-sm-12">
                <img src="<?php echo AOGATAX_IMAGES . 'qa.png'; ?>" />
                <?php 
                    $query = new WP_Query( array('post_type' => 'home', 'name' => 'qa' ) );
                    while ( $query->have_posts() ) : $query->the_post();
                        the_title('<h2>', '</h2>');
                        ?>
                            <div class="aogatax-issue-content">
                                <?php
                                    the_title('<h2>', '</h2>'); 
                                    the_content(); 
                                ?>
                                <a href="qa">Learn More</a>
                            </div>
                        <?php 
                    endwhile;
                ?>

            </div>
            <div class="aogatax-issues-content col-xs-12">
                <div class="text-center"></div>
            </div>
        </div>
        

        <div class="aogatax-signatures aogatax-section col-xs-12">
            
            <?php 
                $query = new WP_Query( array('post_type' => 'home', 'pagename' => 'signature-text' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
            
                // begin signatures 
                $search_criteria = array( 'status' => 'active' );
                $sorting         = array();
                $paging          = array( 'page_size' => 1000000 );
                $entries         = GFAPI::get_entries( 1, $search_criteria, $sorting, $paging );

                $num_sig         = GFAPI::count_entries( 1, $search_criteria );

                foreach($entries as $entry) {
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="aogatax-signature">
                            <?php

                                echo '<h2>' . substr($entry[1],0,1) . '.' . substr($entry[3],0,1) . '</h2>';
                                echo '<p>' . $entry[6] . ', ' . $entry[7] . '</p>';
                                echo '<p>' . date( 'F d, Y', strtotime($entry['date_created']) ) . '</p>';
                                echo '<p> Signature #' . $num_sig-- . '</p>';
                
                            ?>
                        </div>
                    </div>
                    <?php
                }

            ?>
        </div>
    
    </main>

<?php get_footer();