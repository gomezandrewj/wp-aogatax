<?php
/**
 * Default Site Header
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<title><?php wp_title(bloginfo('name').' |',true); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<div class="page">
			<?php do_action( 'body_opening' ); ?>

			<header class="site-header col-lg-12">
				<img class="header" src="<?php echo AOGATAX_IMAGES . 'header.png'; ?>" width="100%"/>
				<div class="share">
					<div class="pull-right">
						<img id="share" title="Facebook" alt="Share on Facebook" 
							href="http://www.facebook.com/sharer.php?u=<?php echo site_url(); ?>" 
							src="<?php echo AOGATAX_IMAGES . 'Facebook.png'; ?>" />
						<img id="share" title="Share on LinkedIn" 
							href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url(); ?>&title=AOGA Tax Credits&summary=Keep Alaska “Open for Business”" 
							src="<?php echo AOGATAX_IMAGES . 'LinkedIn.png'; ?>" />
					<?php if (is_front_page()) { ?>
						</div>
						<div>
					<?php } ?>
						<img class="twitter" id="share" title="Share on Twitter" 
							href="http://twitter.com/share?url=<?php echo site_url(); ?>&amp;text=Low oil prices could cost North America's oil industry a collective $2 billion a WEEK. #AKLeg" 
							src="<?php echo AOGATAX_IMAGES . 'Twitter.png'; ?>" />
						<?php if (is_front_page()) { 
							AOGATAX\Theme\getTweet();
						 } ?>
					</div>
				</div>

				<?php if(!is_front_page()) { ?>
					<div class="menu">
						<?php wp_nav_menu( array( 'theme_location' => 'header-menu' )); ?>
					</div>
				<?php } ?>

			</header>

