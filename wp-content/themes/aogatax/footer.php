<?php
/**
 * The Default Site Footer
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */
?>

		<footer>
            
            <?php if (is_front_page()) { ?>
                <div class="popup-question-div">
                <table class="popup-question-imgs">
                <tbody>
                <tr>
                <td class="popup-submit-question-top" width="84%"><img src="<?php echo AOGATAX_IMAGES . 'popup-question.png'; ?>" /></td>
                <td class="popup-close-question-top" width="16%"><img src="<?php echo AOGATAX_IMAGES . 'popup-close.png'; ?>" /></td>
                </tr>
                </tbody>
                </table>
                <?php gravity_form('Submit Your Own Question - Pop Up', false, false, false, '', false); ?>
                </div>
            <?php 
            } 

			wp_footer(); ?>

            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-73099220-1', 'auto');
              ga('send', 'pageview');

            </script>

            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5PG3KR"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5PG3KR');</script>
		</footer>
	</div>
</body>
</html>