<?php
/**
 * AOGATAX functions and definitions
 *
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */
namespace AOGATAX\Theme;

// Useful global constants
define( 'AOGATAX_VERSION',      '0.1.0' );
define( 'AOGATAX_TEMPLATE_URL', get_template_directory_uri() );
define( 'AOGATAX_PATH',         get_template_directory() . '/' );
define( 'AOGATAX_INC',          AOGATAX_PATH . 'includes/' );
define( 'AOGATAX_ASSETS',		 AOGATAX_TEMPLATE_URL . '/assets/' );
define( 'AOGATAX_IMAGES',		 AOGATAX_TEMPLATE_URL . '/assets/images/' );

// Include files
require_once AOGATAX_INC . 'setup.php';
require_once AOGATAX_INC . 'gform_mail.php';
require_once 'vendor/autoload.php';

// Load Files
add_action( 'after_setup_theme', 'AOGATAX\Theme\Setup\setup' );
add_action( 'after_setup_theme', 'AOGATAX\Theme\Mail\setup' );

// Global functions

/*
 * http://www.sanwebe.com/2014/07/get-latest-twitter-status-easily-with-php
 */
function getTweet() {
    $twitter_customer_key           = 'ZprogMRgpWxhxa4tXaCALDjbd';
    $twitter_customer_secret        = 'J3rRnGQQy4TloL8jN16IJ8CV61LbqsyhL1fUPLUT4X68L5PiDi';
    $twitter_access_token           = '1449289915-Tbl28BFFUUZ5JqdRV78mCyUvvJAbrMGJZd1J4cQ';
    $twitter_access_token_secret    = 'vVMxAZb4aFjAWSEBipjNkhub1y4KRxH4qB3zRB2ffWN39';

    $connection = new \Abraham\TwitterOAuth\TwitterOAuth($twitter_customer_key, $twitter_customer_secret, $twitter_access_token, $twitter_access_token_secret);

    $my_tweets = $connection->get('statuses/user_timeline', array('screen_name' => 'aoga', 'count' => 1));

    echo makeClickableLinks($my_tweets[0]->text);
}

function makeClickableLinks($s) {
    return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a target="blank" rel="nofollow" href="$1" target="_blank">$1</a>', $s);
}