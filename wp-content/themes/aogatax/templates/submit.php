<?php
/**
 * Template Name: Form Submit
 *
 * @package AOGATAX
 * @subpackage Theme
 * @since 0.1.0
 */

get_header(); ?>

    <div class="clearfix"></div>
    <main class="aogatax-main">

        <div class="aogatax-section">

        <?php 

            $total_sigs = GFAPI::count_entries(1);

            if (!empty($_GET) && $_GET['sig'] > 0) { ?>

                <div class="aogatax-success text-center">
                    <?php while ( have_posts() ) : the_post();
                        the_content();
                    endwhile; ?>
                </div>

        <?php } ?>

        </div>

        <div class="aogatax-signatures aogatax-section col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
            <?php 
                $query = new WP_Query( array('post_type' => 'page', 'pagename' => 'signature-text' ) );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
                
                $num_sigs = 24;
            
                // begin signatures 
                $search_criteria = array( 'status' => 'active' );
                $sorting         = array();
                $paging          = array( 'page_size' => $num_sigs );
                $entries         = GFAPI::get_entries( 1, $search_criteria, $sorting, $paging );

                $num_sig         = GFAPI::count_entries( 1, $search_criteria );

                foreach($entries as $entry) {
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="aogatax-signature">
                            <?php

                                echo '<h2>' . substr($entry[1],0,1) . '.' . substr($entry[3],0,1) . '</h2>';
                                echo '<p>' . $entry[6] . ', ' . $entry[7] . '</p>';
                                echo '<p>' . date( 'F d, Y', strtotime($entry['date_created']) ) . '</p>';
                                echo '<p> Signature #' . $num_sig-- . '</p>';
                
                            ?>
                        </div>
                    </div>
                    <?php
                }

            ?>
        </div>

    </main>

<?php get_footer(); 